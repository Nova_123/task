<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CURA Healthcare_menu-toggle</name>
   <tag></tag>
   <elementGuidId>46f80aa4-1cec-4186-991a-439b6155fab7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#menu-toggle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>792daea4-d7bb-4704-a536-bec8c6a08fda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-toggle</value>
      <webElementGuid>c8c056cb-7462-40f0-b7ec-175fe53c76e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>937b8366-46ff-4c29-adb4-807549676ddd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-dark btn-lg toggle</value>
      <webElementGuid>682f0e30-4aff-419e-8e18-666f5f5d39d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)</value>
      <webElementGuid>5884eddd-e254-4538-a998-a84b8c85380d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']</value>
      <webElementGuid>69dcbf61-89a1-4905-a569-250aca0ca85b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare'])[1]/preceding::a[2]</value>
      <webElementGuid>06a8270b-1be0-45bb-ba47-e1ea4df7d56b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::a[3]</value>
      <webElementGuid>1c77b23c-ebdc-4642-bffa-8ac9cf672f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>1f9e29e9-e69e-4e51-85d5-cad6d9edbe3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>4967f8cd-bc71-4f56-a2cf-a6f0f50fd3b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'menu-toggle' and @href = '#']</value>
      <webElementGuid>d31bb398-5c1a-42ee-9dad-9659768891a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
