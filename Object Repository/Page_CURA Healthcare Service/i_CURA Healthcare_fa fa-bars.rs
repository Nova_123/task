<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_CURA Healthcare_fa fa-bars</name>
   <tag></tag>
   <elementGuidId>9ca9bdbf-b658-4802-931b-33e386270eb9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-bars</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>380efcf4-4fdb-4d1b-98bd-c987ba4a5c78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-bars</value>
      <webElementGuid>248bf015-a755-457c-8eb2-19dae6096502</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)/i[@class=&quot;fa fa-bars&quot;]</value>
      <webElementGuid>e78e4aad-8bad-4afa-a922-b38a1241d042</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']/i</value>
      <webElementGuid>8d7d2ade-687d-4691-972d-ebace3427155</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//i</value>
      <webElementGuid>973b0028-7660-442d-8c93-e86137067dd4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
